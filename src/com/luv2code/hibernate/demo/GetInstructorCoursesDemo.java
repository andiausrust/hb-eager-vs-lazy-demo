package com.luv2code.hibernate.demo;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class GetInstructorCoursesDemo {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			
			// get the instructor from db
			 Instructor instructor = session.get(Instructor.class, 1);
			
			// create some courses
			 Course course1 = new Course("The final");
			 Course course2 = new Course("teh end");
			
			// add courses to instructor
			instructor.add(course1);
			instructor.add(course2);
			 
			// save the courses
			session.save(course1);
			session.save(course2);

			session.getTransaction().commit();
			
		} finally {
			session.close();
			factory.close();
		}
		
	}

}
 